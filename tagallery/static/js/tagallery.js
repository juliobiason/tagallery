/**
 * Recalculate the number of elements that must be displayed in
 * the screen and update the page count.
 */
function recalc(image_count, thumbsize) {
  var height = $(window).height() - $('nav.top-bar').height();
  var width = $('div#content').width();

  console.log(width + ' x ' + height);

  var across = Math.floor(width / (thumbsize + 20));    // 20 for padding
  var along = Math.floor(height / (thumbsize + 20));    // again, 20 for padding

  console.log(across + ' images across and ' + along + ' images along');
  
  var total = across * along;
  console.log('Total images in page: ' + total);

  for (var i = 0; i < total; ++i) {
    var holder = [
      '<li style="',
      'width:' + thumbsize + 'px;',
      'height:' + thumbsize + 'px',
      '">',
      '</li>'
    ].join('');
    console.log(holder);
    $('#images').append($(holder));
  }
}

/**
 * Display an "alert" at the top of the screen
 */
function warning(message) {
  $('#content').prepend([
    '<div data-alert class="alert-box">',
    message,
    '<a href="#" class="close">&times;</a>',
    '</div>'
  ].join(''));
}
