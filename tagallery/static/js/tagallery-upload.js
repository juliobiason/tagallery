/**
 * Upload functions
 */


/**
 * List of filetypes accepted for uploading
 */
var acceptedTypes = {
  'image/png': true,
  'image/jpeg': true,
  'image/gif': true
};

/**
 */
function setImage(domObj, source) {
  var image = new Image();
  image.onload = function () { 
    console.log('Target = ' + domObj.offsetWidth + ' x' + domObj.offsetHeight);
    console.log('Image = ' + image.width + ' x ' + image.height);

    var propW = domObj.offsetWidth / image.width;
    var propH = domObj.offsetHeight / image.height;

    console.log('Proportion = ' + propW + ' x ' + propH);

    if (image.height * propW > domObj.offsetHeight) {
      image.width = image.width * propH;
    } else {
      image.width = image.width * propW;
    }

    domObj.appendChild(image);
  };
  image.src = source;
}

/**
 * Upload a single file.
 */
function uploadSingleFile(fileObj, filename, url) {
  /* prepare the ajax request */
  var form = new FormData();
  console.log(fileObj);
  form.append('file', fileObj);
  console.log(form);

  var xhr = new XMLHttpRequest();
  xhr.open('POST', url);
  xhr.onload = function () {
    console.log('xhr onload');
    progressBar.classList.add('invisible');
  };

  /* create the template, to have some progress bar for this upload */
  var holder = addImage(filename, null, true);
  var progressBar = holder.querySelector('div.progress');
  var meter = progressBar.querySelector('span.meter');
  meter.width = 0;
  progressBar.classList.remove('invisible');

  /* set the uploading progress */
  xhr.upload.onprogress = function (ev) {
    console.log('xhr.upload.onprogress');
    console.log(ev.loaded);
    console.log(ev.total);
    console.log(ev.loaded / ev.total);
    meter.width = (ev.loaded / ev.total) * 100 + '%';
  };

  /* launch the ajax request */
  xhr.send(form);

  /* update the info in the form */
  holder.querySelector('input[name="filename"]').value = filename;

  /* while the ajax is sending the file, we load the image in the drop zone */
  var reader = new FileReader();
  reader.onload = function (ev) {
    setImage(holder.querySelector('div.image'), ev.target.result);
  };
  reader.readAsDataURL(fileObj);
}

/**
 * Read the incoming files, display in the drop zone and start the 
 * XML upload.
 */
function uploadFiles(files, url, after) {
  "use strict";
  after = after || null;
  url = url || "Just uploaded...";

  var uploading = null;
  for (var i = 0; i < files.length; ++i) {
    console.log(files[i].name);
    if (acceptedTypes[files[i].type] !== true) {
      warning('File ' + files[i].name + ' ignored, not a valid type (' + files[i].type + ')');
      continue;
    }

    uploading = files[i];
    uploadSingleFile(files[i], files[i].name, url);
  }
}

/**
 * Add a ne wimage holder
 */
function addImage(filename, source, upload) {
  "use strict";
  upload = upload || false;
  source = source || false;

  var original = document.querySelector('#upload div.template');
  var template = original.cloneNode(true);
  template.classList.remove('template');
  template.classList.remove('hide');
  if (upload) {
    template.classList.add('just-uploaded');
  }

  // document.querySelector('#upload').appendChild(template);
  var anchor = document.querySelector('#upload div.new');
  if (anchor && anchor.nextSibling) {
    original.parentNode.insertBefore(template, anchor.nextSibling);
  } else {
    original.parentNode.appendChild(template);
  }
  // document.querySelector('#upload div.template').parentNode()

  var title = template.querySelector('div.image-title');
  title.innerHTML = filename;

  var target = template.querySelector('div.image');
  console.log('----------------------');

  if (source) {
    setImage(target, source);
  }

  return template;
}

/**
 * Retrieve information about the images in the incoming folder
 */
function getQueue(url) {
  "use strict";

  var xhr = new XMLHttpRequest();
  xhr.open('get', url);
  xhr.onload = function () {
    var response = JSON.parse(this.responseText);
    console.log(response);

    if (response.status == 'OK') {
      for (var pos in response.files) {
        addImage(response.files[pos].title, response.files[pos].url);
      }
    }
  };
  xhr.send();
}
