#!/usr/bin/env python
# -*- encoding: utf-8 -*-

#   Tagallery, a tag based web gallery
#   Copyright (C) 2014  Julio Biason
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Handle storing and retrieving images in the file system."""

import datetime
import os.path

from database import Image

from flask import current_app


def partition(image):
    """Return the partition for the file in the filesystem. This returns the
    storage path without the configuration storage path.

    Although it's not returned in the result, this function will actually
    check if the returned direction already exists for new images."""
    path = None
    if isinstance(image, basestring):
        # we only have a filename, which means we don't know when the file was
        # saved (or, better yet, if it was), so we return where this should be
        # stored, considering it was uploaded today

        today = datetime.date.today()
        path = os.path.join(
            today.strftime('%Y'),
            today.strftime('%m'),
            today.strftime('%d'))

        fullpath = os.path.join(current_app.config['IMAGE_PATH'], path)
        try:
            os.makedirs(fullpath)
        except OSError:
            pass    # already exists, so it's ok
    elif isinstance(image, Image):
        # with an Image object, we have the uploaded date, so we can extract
        path = image.filename

    return path


def thumbname(filename):
    """Return the thumbnail name of an image. The filename must be a fully
    partitioned name."""
    partition = os.path.dirname(filename)
    filename = os.path.basename(filename)

    fileparts = os.path.splitext(filename)
    return os.path.join(partition,
                        'thumb-' + fileparts[0] + fileparts[1])
