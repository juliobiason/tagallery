#!/usr/bin/env python
# -*- encoding: utf-8 -*-

#   Tagallery, a tag based web gallery
#   Copyright (C) 2014  Julio Biason
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""The main page."""

import logging

from flask import Blueprint
from flask import render_template
from flask import current_app

from pony.orm import count
from pony.orm import select

from tagallery.database import Image

log = logging.getLogger('tragallery.index')

index = Blueprint('index',
                  __name__)


@index.route('/', defaults={'page': 1})
@index.route('/<int:page>')
def all_images(page):
    return render_template('index.html',
                           total_images=_image_count(),
                           thumbnail_size=current_app.config['THUMBNAIL_SIZE'])


def _image_count():
    """Return the number of images, either from the db or in the app
    "cache"."""
    total_images = current_app.config['_IMAGES']
    if not total_images:
        total = list(select(count(record) for record in Image))
        log.debug(total)
        total_images = total[0]
        current_app.config['_IMAGES'] = total_images

    log.debug('{0} images in the database'.format(total_images))
    return total_images
