#!/usr/bin/env python
# -*- encoding: utf-8 -*-

#   Tagallery, a tag based web gallery
#   Copyright (C) 2014  Julio Biason
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Image upload module."""

import os
import os.path
import datetime
import logging

from werkzeug import secure_filename

from flask import Blueprint
from flask import render_template
# from flask import redirect
from flask import url_for
from flask import current_app
from flask import request
from flask import jsonify
from flask import flash
from flask import abort
from flask import send_from_directory

from flask.ext.login import login_required

from PIL import Image as ImageFile

from tagallery.database import Image
from tagallery.database import Tag

from tagallery.storage import partition
from tagallery.storage import thumbname

upload = Blueprint('upload', __name__)

LOG = logging.getLogger('tagallery.blueprints.upload')


@upload.route('/', methods=['GET'])
@login_required
def dashboard():
    return render_template('upload/upload.html')
    # return redirect(url_for("upload.from_browser"))
    # return render_template('upload/dashboard.html')


@upload.route('/browser/upload/', methods=['POST'])
@login_required
def upload_file():
    fileobj = request.files['file']
    response = {'status': 'UNKNOWN', 'filename': None}
    if fileobj:
        filename = secure_filename(fileobj.filename)
        path = os.path.join(current_app.config['IMAGE_PATH'],
                            partition(filename))
        try:
            os.makedirs(path)
        except os.error:
            pass    # already exists

        final_filename = os.path.join(path, filename)
        fileobj.save(final_filename)
        response['status'] = 'OK'
        response['filename'] = final_filename

        LOG.debug('File {file} saved on disk'.format(file=final_filename))

    return jsonify(response)


@upload.route('/save/', methods=['POST'])
@login_required
def save():
    filename = request.values.get('filename')
    title = request.values.get('title')
    form_tags = request.values.get('tags', '').split(',')

    if len(form_tags) == 0:
        flash('Must have at least one tag', 'alert')

    tags = []
    for single_tag in form_tags:
        if not single_tag:
            continue

        LOG.debug('Searching for tag {0}'.format(single_tag))
        tag = Tag.get(tag=single_tag)
        if not tag:
            LOG.debug('Tag {0} not found, creating new'.format(single_tag))
            tag = Tag(tag=single_tag)
        tags.append(tag)

    Image(title=title,
          tags=tags,
          created_at=datetime.datetime.utcnow(),
          filename=filename)

    LOG.debug('Image saved')

    thumb = ImageFile.open(filename)
    size = current_app.config['THUMBNAIL_SIZE']
    thumb.thumbnail((size, size),
                    ImageFile.ANTIALIAS)
    thumb.save(thumbname(filename))

    LOG.debug('Thumbnail {0} created'.format(thumbname(filename)))

    flash('Upload completed with success')

    return render_template('upload/from_browser.html', source='browser')


@upload.route('/queue/', methods=['GET'])
@login_required
def queue():
    """Return a list of files waiting in the incoming directory."""
    root = os.path.expanduser(current_app.config['IMAGE_QUEUE'])
    LOG.debug('Queue dir: {0}'.format(root))
    result = {'status': 'OK', 'files': []}

    try:
        for filename in os.listdir(root):
            LOG.debug('Found file: {0}'.format(filename))

            # pruning unwanted files
            if not filename.lower().endswith('.png') and \
                    not filename.lower().endswith('.jpg') and \
                    not filename.lower().endswith('.gif'):
                continue

            result['files'].append({
                'url': url_for('upload.serve_queue', filename=filename),
                'title': filename
            })
    except OSError:
        # the queue directory does not exist
        pass

    return jsonify(result)


@upload.route('/queue/image/', methods=['GET'])
@login_required
def serve_queue():
    """Serve an image directly from the queue."""
    filename = request.values.get('filename')
    if not filename:
        abort(400)

    queue_dir = os.path.expanduser(current_app.config['IMAGE_QUEUE'])

    return send_from_directory(queue_dir, filename)
