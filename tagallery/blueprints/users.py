#!/usr/bin/env python
# -*- encoding: utf-8 -*-

#   Tagallery, a tag based web gallery
#   Copyright (C) 2014  Julio Biason
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""User management."""

from flask import Blueprint
from flask import render_template
from flask import request
from flask import flash
from flask import redirect
from flask import url_for

from flask.ext.login import login_user
from flask.ext.login import logout_user

from tagallery.database import User

# from pony.orm import db_session
from pony.orm import ObjectNotFound

import logging
import crypt

users = Blueprint('users', __name__)

log = logging.getLogger('tagallery.users')


@users.route('/login/', methods=['GET'])
def show_login():
    return render_template('users/login.html')


@users.route('/login/', methods=['POST'])
def make_login():
    username = request.values.get('username')
    password = request.values.get('password')

    if not username:
        log.debug('Missing username')
        flash('Username is required', 'alert')
        return render_template('users/login.html', username=username)

    if not password:
        log.debug('Missing password')
        flash('Password is required', 'alert')
        return render_template('users/login.html', username=username)

    salt = username[0] + username[-1]

    crypted = crypt.crypt(password, salt)
    user = None
    try:
        user = User.get(login=username, password=crypted)
    except ObjectNotFound:
        user = None

    log.debug('User = {0}'.format(user))
    if not user:
        flash('Invalid username/password', 'alert')
        return render_template('users/login.html', username=username)

    login_user(user, remember=True)
    return redirect(request.args.get('next') or url_for('index.all_images'))


@users.route('/logout/', methods=['GET'])
def logout():
    logout_user()
    return redirect(url_for('index.all_images'))
