#!/usr/bin/python
# -*- encoding: utf-8 -*-

#   Tagallery, a tag based web gallery
#   Copyright (C) 2014  Julio Biason
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Database definitions and helpers."""

import datetime

from pony.orm import Database
from pony.orm import PrimaryKey
from pony.orm import Optional
from pony.orm import Required
from pony.orm import Set

from flask.ext.login import UserMixin


# configs will be useful when Pony 0.5 is released
db = Database("sqlite", "tagallery.db", create_db=True)


class Image(db.Entity):
    """A single image."""
    id = PrimaryKey(int, auto=True)
    title = Optional(unicode)
    tags = Set("Tag")
    created_at = Required(datetime.datetime)
    filename = Required(str, unique=True)


class Tag(db.Entity):
    """One tag."""
    id = PrimaryKey(int, auto=True)
    tag = Required(unicode)
    images = Set(Image)


class User(db.Entity, UserMixin):
    """A system user."""
    login = Required(str, unique=True)
    password = Required(str)

db.generate_mapping(create_tables=True)

# 	select
# 		images.title
# 	from
# 		images,
# 		tagged,
# 		tags
# 	where
# 		images.uid == tagged.image
# 		AND tagged.tag == tags.uid
# 		AND tags.desc in (:list)
# 	GROUP BY
# 		images.title
# 	HAVING
# 		count(distinct tags.uid) >= :count
