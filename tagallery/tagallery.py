#!/usr/bin/env python
# -*- encoding: utf-8 -*-

#   Tagallery, a tag based web gallery
#   Copyright (C) 2014  Julio Biason
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Flask app definition, configuration and setup."""

import os.path
import logging

from flask import Flask

from flask.ext.login import LoginManager

from database import User
# from database import start

# ----------------------------------------------------------------------
# Config
# ----------------------------------------------------------------------

#: Where images are saved
IMAGE_PATH = os.path.expanduser('~/tagallery/images')

#: Queued images
IMAGE_QUEUE = os.path.expanduser('~/tagallery/incoming')

#: Thumbnail size (thumbnail size is squared, but with proportions kept)
THUMBNAIL_SIZE = 200

#: Secret phrase for encrypting the session
SECRET_KEY = 'TagallerySecretKey'

#: If you're using SQLite, set this to the database filename; otherwise, leave
#: it blank
SQLITE_FILENAME = None

#: If you're using MySQL/MariaDB, set these settings to connect to your
#: database; otherwise, leave it blank
MYSQL_HOST = None
MYSQL_USER = None
MYSQL_PASSWORD = None
MYSQL_DATABASE = None

#: PRIVATE: Number of images in the database (so we don't need to keep
#: requesting the count every time.)
_IMAGES = 0


# ----------------------------------------------------------------------
# Config
# ----------------------------------------------------------------------
app = Flask(__name__)
app.config.from_object(__name__)
app.config.from_envvar('TAGALLERY_CONFIG', True)

# ----------------------------------------------------------------------
# Database
# ----------------------------------------------------------------------
from pony.orm import db_session
from pony.orm import ObjectNotFound
# start(app)


# ----------------------------------------------------------------------
# blueprints
# ----------------------------------------------------------------------
from blueprints.index import index
from blueprints.users import users
from blueprints.admin import admin
from blueprints.upload import upload

app.register_blueprint(index, url_prefix='/')
app.register_blueprint(users, url_prefix='/user')
app.register_blueprint(admin, url_prefix='/admin')
app.register_blueprint(upload, url_prefix='/upload')

login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = 'users.show_login'

# wrap the whole application in the a db session
app.wsgi_app = db_session(app.wsgi_app)


# user loading (required by pony)
@login_manager.user_loader
def load_user(userid):
    try:
        with db_session:
            user = User[userid]
            return user
    except ObjectNotFound:
        return None

# for running as dev
if __name__ == '__main__':
    log = logging.getLogger('tagallery.server')
    log.debug('Running as devserver...')

    app.run(debug=True)
