About Tagallery
===============

The idea behind Tagallery is to create an image gallery which have no
albums directly. Albums "appear" by tagging images and more specific
albums can be created by selecting multiple related tags.

The principle is not that different from the way Flickr works, although
Flickr have support for albums (or "sets").

Libraries
---------

Currently, Tagallery requires Flask, Flask-Login, Flask-Script and PonyORM.

Revisions
---------

THERE IS NO CURRENT VERSION OF TAGALLERY. Everything here is pre-alpha and
shouldn't be even considered ready-to-use.
