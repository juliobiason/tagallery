#!/usr/bin/env python
# -*- encoding: utf-8 -*-

#   Tagallery, a tag based web gallery
#   Copyright (C) 2014  Julio Biason
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.

import logging

from flask.ext.script import Manager
# from flask.ext.script import option

from tagallery.tagallery import app

manager = Manager(app)


# adduser
@manager.command
@manager.option('-u', '--user', dest='user')
@manager.option('-p', '--password', dest='password')
def adduser(user, password):
    """Add a new user to the database."""
    if not user or not password:
        print 'Username and password are required.'

    import crypt
    from tagallery.database import User

    from pony.orm import db_session
    from pony.orm import commit

    salt = user[0] + user[-1]
    cyphered = crypt.crypt(password, salt)

    with db_session:
        User(login=user, password=cyphered)
        commit()

if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    manager.run()
